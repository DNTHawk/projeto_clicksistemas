## Sumário
<!--ts-->
* [Introdução](#Introdução)
* [Usuário](#1-Usuário)
    * [POST (Login Usuário)](#11-POST-Login-Usuário)
    * [POST (Cadastro de Usuário)](#12-POST-Cadastro-de-Usuário)
    * [GET (Selecionar Todos os Usuário)](#13-GET-Selecionart-Todos-os-Usuário)
    * [GET (Selecionar Usuário pelo ID)](#14-GET-Selecionart-Usuário-pelo-ID)
    * [DELETE (Deletar Usuário pelo ID)](#15-DELETE-Deletar-Usuário-pelo-ID)
* [Cliente](#2-Cliente)
    * [POST (Cadastro de Cliente)](#21-POST-Cadastro-de-Cliente)
    * [PUT (Update de Cliente)](#22-POST-Update-de-Cliente)
    * [GET (Selecionar Todos os Cliente)](#23-GET-Selecionart-Todos-os-Cliente)
    * [GET (Selecionar Cliente pelo ID)](#24-GET-Selecionart-Cliente-pelo-ID)
    * [DELETE (Deletar Cliente pelo ID)](#25-DELETE-Deletar-Cliente-pelo-ID)
* [Filial](#3-Filial)
    * [POST (Cadastro de Filial)](#31-POST-Cadastro-de-Filial)
    * [PUT (Update de Filial)](#32-POST-Update-de-Filial)
    * [GET (Selecionar Todos os Filial)](#33-GET-Selecionart-Todos-os-Filial)
    * [GET (Selecionar Filial pelo ID)](#34-GET-Selecionart-Filial-pelo-ID)
    * [DELETE (Deletar Filial pelo ID)](#35-DELETE-Deletar-Filial-pelo-ID)
* [Administrar do Sistema](#4-Administrar-do-Sistema)
    * [POST (Cadastro de Administrador)](#41-POST-Cadastro-de-Administrador)
    * [PUT (Update de Administrador)](#42-POST-Update-de-Administrador)
    * [GET (Selecionar Todos os Administradores)](#43-GET-Selecionart-Todos-os-Administradores)
    * [GET (Selecionar Administrador pelo ID)](#44-GET-Selecionart-Administrador-pelo-ID)
    * [DELETE (Deletar Administrador pelo ID)](#45-DELETE-Deletar-Administrador-pelo-ID)
* [Produto](#4-Produto)
    * [POST (Cadastro de Produto)](#41-POST-Cadastro-de-Produto)
    * [PUT (Update de Produto)](#42-POST-Update-de-Produto)
    * [GET (Selecionar Todos os Produtos)](#43-GET-Selecionart-Todos-os-Produtos)
    * [GET (Selecionar Produto pelo ID)](#44-GET-Selecionart-Produto-pelo-ID)
    * [DELETE (Deletar Produto pelo ID)](#45-DELETE-Deletar-Produto-pelo-ID)

Introdução 
========
> Seguem abaixo informações de como fazer requisições no backend.  
Ex: 
```sh
http://localhost:3000/usuario/cadastrar
```
ou
```sh
http://UrlDoServidor:3000/usuario/cadastrar
```
> Obs: Todas as requisições precisam de token (Exceto o Login).

## Rotas

## 1 Usuário
#### 1.1 POST Login Usuário
Rota
```
/query/authenticateLogin
```
Json
```
{
	"email" : "lipedigo@hotmail.com",
	"password" : "123Mudar"
}
```
#### 1.2 POST Cadastro de Usuário
Rota
```
/user/register
```
Json
```
{
	"name" : "Felipe Rodrigo",
	"email" : "lipedigo@hotmail.com",
	"password" : "123Mudar",
	"access" : "adm",
	"idAccess": "5d95f353eac270945f5f5fc1"
}
```

> Obs: No caso do cadastro de Filial.

Json
```
{
	"name" : "Felipe Rodrigo",
	"email" : "lipedigo@hotmail.com",
	"password" : "123Mudar",
	"access" : "adm",
	"idAccess": "5d95f353eac270945f5f5fc1",
	"idClient": "5d95f353eac270945f5f5fc1"
}
```

Retorno Json
```
{
    "user": {
        "_id": "5d95f376eac270945f5f5fc2",
        "name": "Felipe Rodrigo",
        "email": "lipedigo@hotmail.com",
        "access": "adm",
        "idAdm": "5d95f353eac270945f5f5fc1",
        "createdAt": "2019-10-03T13:11:18.409Z",
        "__v": 0
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkOTVmMzc2ZWFjMjcwOTQ1ZjVmNWZjMiIsImFjY2VzcyI6ImFkbSIsImlkQWNjZXNzIjoiNWQ5NWYzNTNlYWMyNzA5NDVmNWY1ZmMxIiwiaWF0IjoxNTcwMTA4Mjc4LCJleHAiOjE1NzAxOTQ2Nzh9.71SvqkRzCAo1DFpjIUFRAI9F_r7FkMwwlFTsmhMMjiM"
}
```
#### 1.3 GET Selecionar todos os Usuário
Rota
```
/user/selectAll
```
Retorno Json
```
{
    "users": [
        {
            "_id": "5d95f376eac270945f5f5fc2",
            "name": "Felipe Rodrigo",
            "email": "lipedigo@hotmail.com",
            "access": "adm",
            "idAdm": {
                "_id": "5d95f353eac270945f5f5fc1",
                "name": "ClickSistemas",
                "createdAt": "2019-10-03T13:10:43.462Z",
                "__v": 0
            },
            "createdAt": "2019-10-03T13:11:18.409Z",
            "__v": 0
        }
    ]
}
```
#### 1.4 GET Selecionar Usuário pelo ID
Rota
```
/user/selectById/5d95f376eac270945f5f5fc2
```
Retorno Json
```
{
    "user": {
        "_id": "5d95f376eac270945f5f5fc2",
        "name": "Felipe Rodrigo",
        "email": "lipedigo@hotmail.com",
        "access": "adm",
        "idAdm": {
            "_id": "5d95f353eac270945f5f5fc1",
            "name": "ClickSistemas",
            "createdAt": "2019-10-03T13:10:43.462Z",
            "__v": 0
        },
        "createdAt": "2019-10-03T13:11:18.409Z",
        "__v": 0
    }
}
```
#### 1.5 DELETE Deletar Usuário pelo ID
Rota
```
/user/deleteById/5d95f61da25f3794dcd4faa1
```

## 2 Cliente
#### 2.1 POST Cadastro de Cliente
Rota
```
/client/register
```
Json
```
{
	"name": "Supermercado Camilo"
}
```

Retorno Json
```
{
    "client": {
        "_id": "5d9616b40573a797ffcfd4dd",
        "name": "Supermercado Camilo",
        "createdAt": "2019-10-03T15:41:40.107Z",
        "__v": 0
    }
}
```
#### 2.2 PUT Update de Cliente
Rota
```
/client/update/5d9616b40573a797ffcfd4dd
```
Json
```
{
	"name": "Supermercado Camilo2"
}
```

Retorno Json
```
{
    "client": {
        "_id": "5d9616b40573a797ffcfd4dd",
        "name": "Supermercado Camilo2",
        "createdAt": "2019-10-03T15:41:40.107Z",
        "__v": 0
    }
}
```
#### 2.3 GET Selecionar todos os Clientes
Rota
```
/client/selectAll
```

Retorno Json
```
{
    "clients": [
        {
            "_id": "5d9614300573a797ffcfd4d9",
            "name": "Supermercado Bom Dia",
            "createdAt": "2019-10-03T15:30:56.515Z",
            "__v": 0
        }
    ]
}
```
#### 2.4 GET Selecionar Cliente pelo ID
Rota
```
/client/selectById/5d9614300573a797ffcfd4d9
```

Retorno Json
```
{
    "client": {
        "_id": "5d9614300573a797ffcfd4d9",
        "name": "Supermercado Bom Dia",
        "createdAt": "2019-10-03T15:30:56.515Z",
        "__v": 0
    }
}
```
#### 2.5 DELETE Deletar Cliente pelo ID
Rota
```
/client/deleteById/5d9614300573a797ffcfd4d9
```

## 3 Filial
#### 3.1 POST Cadastro de Filial
Rota
```
/subsidiary/register
```
Json
```
{
	"name": "Atacado Paiçandu",
	"client" : "5d9616b40573a797ffcfd4dd"
}
```

Retorno Json
```
{
    "subsidiary": {
        "_id": "5d961f61ae90629a402964cf",
        "name": "Atacado Paiçandu",
        "cliente": "5d9616b40573a797ffcfd4dd",
        "createdAt": "2019-10-03T16:18:41.694Z",
        "__v": 0
    }
}
```
#### 3.2 PUT Update de Filial
Rota
```
/subsidiary/update/5d961f61ae90629a402964cf
```
Json
```
{
	"name": "Atacado Paiçandu2",
	"client" : "5d9616b40573a797ffcfd4dd"
}
```

Retorno Json
```
{
    "subsidiary": {
        "_id": "5d961f61ae90629a402964cf",
        "name": "Atacado Paiçandu2",
        "client": "5d9616b40573a797ffcfd4dd",
        "createdAt": "2019-10-03T16:18:41.694Z",
        "__v": 0
    }
}
```
#### 3.3 GET Consultar todas as Filiais
Rota
```
/subsidiary/selectAll
```

Retorno Json
```
{
    "subsidiarys": [
        {
            "_id": "5d9624f667f4d49b48754682",
            "name": "Sarandi - Centro",
            "client": {
                "_id": "5d96143e0573a797ffcfd4da",
                "name": "Supermercado Cidade Canção",
                "createdAt": "2019-10-03T15:31:10.556Z",
                "__v": 0
            },
            "createdAt": "2019-10-03T16:42:30.983Z",
            "__v": 0
        }
    ]
}
```
#### 3.4 GET Consultar Filial pelo ID
Rota
```
/subsidiary/selectById/5d96251167f4d49b48754685
```

Retorno Json
```
{
    "subsidiary": {
        "_id": "5d96251167f4d49b48754685",
        "name": "Paiçandu - Atacado",
        "client": {
            "_id": "5d9616b40573a797ffcfd4dd",
            "name": "Supermercado Camilo",
            "createdAt": "2019-10-03T15:41:40.107Z",
            "__v": 0
        },
        "createdAt": "2019-10-03T16:42:57.520Z",
        "__v": 0
    }
}
```
#### 3.4 DELETE Deletar Filial pelo ID
Rota
```
/subsidiary/deleteById/5d96251167f4d49b48754685
```

## 4 Administrador do Sistema
#### 4.1 POST Cadastro de Administrador
Rota
```
/admSystem/register
```
Json
```
{
	"name": "ClickSistemas"
}
```

Retorno Json
```
{
    "admSystem": {
        "_id": "5d962f679874ff9d268cb9f3",
        "name": "ClickSistemas",
        "createdAt": "2019-10-03T17:27:03.909Z",
        "__v": 0
    }
}
```
#### 4.2 POST Update de Administrador
Rota
```
/admSystem/register
```
Json
```
{
	"name": "ClickSistemas"
}
```

Retorno Json
```
{
    "admSystem": {
        "_id": "5d962f679874ff9d268cb9f3",
        "name": "ClickSistemas",
        "createdAt": "2019-10-03T17:27:03.909Z",
        "__v": 0
    }
}
```
#### 4.3 Selecionar todos os Administradores
Rota
```
/admSystem/selectAll
```
Retorno Json
```
{
    "admSystems": [
        {
            "_id": "5d95f353eac270945f5f5fc1",
            "name": "ClickSistemas",
            "createdAt": "2019-10-03T13:10:43.462Z",
            "__v": 0
        }
    ]
}
```
#### 4.4 Selecionar Administrador pelo ID
Rota
```
/admSystem/selectById/5d962f679874ff9d268cb9f3
```
Retorno Json
```
{
    "admSystem": {
        "_id": "5d962f679874ff9d268cb9f3",
        "name": "ClickSistemas2",
        "createdAt": "2019-10-03T17:27:03.909Z",
        "__v": 0
    }
}
```
#### 4.5 Deletar Administrador pelo ID
Rota
```
/admSystem/selectById/5d962f679874ff9d268cb9f3
```

## 5 Produto
#### 5.1 POST Cadastro de Produto
Rota
```
/product/register
```
Json
```
{
	"name" : "Feijão",
	"client" : "5d96143e0573a797ffcfd4da",
	"subsidiary" : "5d9624f667f4d49b48754682"
}
```

Retorno Json
```
{
    "product": {
        "_id": "5d9636299580c09e9d4adec2",
        "name": "Feijão",
        "client": "5d96143e0573a797ffcfd4da",
        "subsidiary": "5d9624f667f4d49b48754682",
        "createdAt": "2019-10-03T17:55:53.976Z",
        "__v": 0
    }
}
```
#### 5.2 POST Update de Produto
Rota
```
/product/update/5d9636299580c09e9d4adec2
```
Json
```
{
	"name" : "Feijão2",
	"client" : "5d96143e0573a797ffcfd4da",
	"subsidiary" : "5d9624f667f4d49b48754682"
}
```

Retorno Json
```
{
    "product": {
        "_id": "5d9636299580c09e9d4adec2",
        "name": "Feijão2",
        "client": "5d96143e0573a797ffcfd4da",
        "subsidiary": "5d9624f667f4d49b48754682",
        "createdAt": "2019-10-03T17:55:53.976Z",
        "__v": 0
    }
}
```
#### 5.3 Selecionar todos os Produtos
Rota
```
/product/selectAll
```
Retorno Json
```
{
    "products": [
        {
            "_id": "5d9636299580c09e9d4adec2",
            "name": "Feijão2",
            "client": "5d96143e0573a797ffcfd4da",
            "subsidiary": "5d9624f667f4d49b48754682",
            "createdAt": "2019-10-03T17:55:53.976Z",
            "__v": 0
        }
    ]
}
```
#### 5.4 Selecionar Produto pelo ID
Rota
```
/product/selectById/5d9636299580c09e9d4adec2
```
Retorno Json
```
{
    "product": {
        "_id": "5d9636299580c09e9d4adec2",
        "name": "Feijão2",
        "client": "5d96143e0573a797ffcfd4da",
        "subsidiary": "5d9624f667f4d49b48754682",
        "createdAt": "2019-10-03T17:55:53.976Z",
        "__v": 0
    }
}
```
#### 5.5 Deletar Produto pelo ID
Rota
```
/product/deleteById/5d9636299580c09e9d4adec2
```