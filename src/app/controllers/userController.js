const express = require('express');
const authMiddleware = require('../middlewares/auth');
const jwt = require('jsonwebtoken');
const authConfig = require('../../config/auth')

const User = require('../models/User');

const router = express.Router();

router.use(authMiddleware);
const cors = require('cors')
router.use(cors);

function generateToken(params = {}){
    return token = jwt.sign(params, authConfig.secret, { expiresIn: 86400} )
}

// Verifica os dados passados pelo Token
router.get('/', async (req, res) => {
    return res.send({ 
        id: req.userId,
        access: req.access,
        idAccess: req.idAccess
    });
})

router.post('/register', async (req, res) => {
    const {name, email, password, access, idAccess, idClient } = req.body;

    try {
        
        if (req.access === 'adm') {
            if (await User.findOne({ email }))
                return res.status(400).send({ error : 'Usuário já existe' })

            if (access === 'adm') {
                var userState = {
                    name: name,
                    email: email,
                    password: password,
                    access: access,
                    idAdm: idAccess
                }
            } else if (access === 'cliente'){
                var userState = {
                    name: name,
                    email: email,
                    password: password,
                    access: access,
                    idClient: idAccess
                }
            } else if (access === 'filial'){
                var userState = {
                    name: name,
                    email: email,
                    password: password,
                    access: access,
                    idFilial: idAccess,
                    idClient: idClient
                }
            }    
            
            const user = await User.create(userState);

            user.password = undefined;

            if (user.access === 'adm') {
                console.log(user.idAdm);
                
                var idAcesso = user.idAdm
            }else if( user.access === 'cliente' ){
                var idAcesso = user.idClient
            }else if( user.access === 'filial' ){
                var idAcesso = user.idSubsidiary
            }

            return res.send({ 
                user,
                token: generateToken({ id: user.id, access: user.access, idAccess: idAcesso }),
            });
        } else if (req.access === 'cliente') {
            if (access == 'adm'){
                return res.status(400).send({ error : 'Cadastro de Administrador apenas para Administradores' })
            }else{
                if (await User.findOne({ email }))
                    return res.status(400).send({ error : 'Usuário já existe' })

                if (access === 'cliente'){
                    var userState = {
                        name: name,
                        email: email,
                        password: password,
                        access: access,
                        idClient: idAccess
                    }
                } else if (access === 'filial'){
                    var userState = {
                        name: name,
                        email: email,
                        password: password,
                        access: access,
                        idFilial: idAccess,
                        idClient: idClient
                    }
                }    

                const user = await User.create(userState);
    
                user.password = undefined;
    
                if( user.access === 'cliente' ){
                    var idAcesso = user.idClient
                }else if( user.access === 'filial' ){
                    var idAcesso = user.idSubsidiary
                }
    
                return res.send({ 
                    user,
                    token: generateToken({ id: user.id, access: user.access, idAccess: idAcesso }),
                });
            }

        }else if (req.access === 'filial') {
            if (access == 'adm'){
                return res.status(400).send({ error : 'Cadastro de Administrador apenas para Administradores' })
            }else  if (access == 'cliente') {
                return res.status(400).send({ error : 'Cadastro de Cliente apenas para Administradores e Clientes' })
            }else{
                if (await User.findOne({ email }))
                    return res.status(400).send({ error : 'Usuário já existe' })

                if (access === 'filial'){
                    var userState = {
                        name: name,
                        email: email,
                        password: password,
                        access: access,
                        idFilial: idAccess,
                        idClient: idClient
                    }
                }    
    
                const user = await User.create(userState);
    
                user.password = undefined;
    
                return res.send({ 
                    user,
                    token: generateToken({ id: user.id, access: user.access, idAccess: user.idSubsidiary }),
                });

            }
        }
        
    } catch (err) {
        console.log(err);
        
        return res.status(400).send({ error: 'Falha no registro' });
    }
});

router.get('/selectAll', async (req, res) =>{
    if (req.access === 'adm') {
        var users = await User.find().populate('idAdm')
    } else if (req.access === 'cliente') {
        const query = {idClient: req.idAccess}
        var users = await User.find(query).populate('idClient')
    } else if (req.access === 'filial') {
        const query = {idFilial: req.idAccess}
        var users = await User.find(query).populate(['idClient','idFilial'])
    }

    return res.send({ users });
});

router.get('/selectById/:userId', async (req, res) =>{
    if (req.access === 'adm') {
        var user = await User.findById(req.params.userId).populate('idAdm')
    } else if (req.access === 'cliente') {
        const query = {idClient: req.idAccess}
        var user = await User.findById(req.params.userId, query).populate('idClient')
    } else if (req.access === 'filial') {
        const query = {idFilial: req.idAccess}
        var user = await User.findById(req.params.userId, query).populate(['idClient','idFilial'])
    }

    return res.send({ user });
});

router.delete('/deleteById/:userId', async (req, res) => {
    try{
        if (req.access === 'adm') {
            await User.findByIdAndRemove(req.params.userId);
        } else if (req.access === 'cliente') {
            const query = {idClient: req.idAccess}
            await User.findByIdAndRemove(req.params.userId, query);
        } else if (req.access === 'filial') {
            const query = {idFilial: req.idAccess}
            await User.findByIdAndRemove(req.params.userId, query);
        }
    
        return res.send({ msg: 'Deletado com Sucesso' });
    } catch (err) {
        console.log(err);
        return res.status(400).send({ error: 'Falha no delete' });
    }
    
});

module.exports = app => app.use('/user', router);