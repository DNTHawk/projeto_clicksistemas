const express = require('express');
const authMiddleware = require('../middlewares/auth');
const jwt = require('jsonwebtoken');
const authConfig = require('../../config/auth')

const Client = require('../models/Client');

const router = express.Router();

router.use(authMiddleware);
const cors = require('cors')
router.use(cors);

function generateToken(params = {}){
    return token = jwt.sign(params, authConfig.secret, { expiresIn: 86400} )
}

// Verifica os dados passados pelo Token
// router.get('/', async (req, res) => {
//     return res.send({ 
//         id: req.userId,
//         access: req.access,
//         idAccess: req.idAccess
//     });
// })

router.post('/register', async (req, res) => {
    try {
        if (req.access === 'adm') {
            var client = await Client.create(req.body)
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode cadastrar um Cliente' });
        } else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode cadastrar um Cliente' });
        }
        
        return res.send({ client });

    } catch (err) {
        return res.status(400).send({ error: 'Falha no Registro' });
    }
});

router.put('/update/:clientId', async (req, res) => {
    try {
        if (req.access === 'adm') {
            var client = await Client.findByIdAndUpdate(req.params.clientId, req.body, {new: true})
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode alterar um Cliente' });
        } else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode alterar um Cliente' });
        }
        
        return res.send({ client });
    } catch (err) {
        return res.status(400).send({ error: 'Falha no Registro' });
    }
});

router.get('/selectAll', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            var clients = await Client.find()
        } else if (req.access === 'cliente') {
            const query = {idClient: req.idAccess}
            var clients = await Client.find(query)
        } else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador e Cliente pode consultar um Cliente' });
        }
        
        return res.send({ clients });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.get('/selectById/:clientId', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            var client = await Client.findById(req.params.clientId)
        } else if (req.access === 'cliente') {
            const query = {idClient: req.idAccess}
            var client = await Client.findById(req.params.clientId, query)
        } else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador e Cliente pode consultar um Cliente' });
        }
        
        return res.send({ client });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.delete('/deleteById/:clientId', async (req, res) => {
    try{
        if (req.access === 'adm') {
            await Client.findByIdAndRemove(req.params.clientId)
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode deletar um Cliente' });
        } else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode deletar um Cliente' });
        }

        return res.send({ msg: 'Deletado com Sucesso' });
    } catch (err) {
        return res.status(400).send({ error: 'Falha no Delete' });
    }
    
});

module.exports = app => app.use('/client', router);