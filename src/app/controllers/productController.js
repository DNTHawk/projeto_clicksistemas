const express = require('express');
const jwt = require('jsonwebtoken');
const authMiddleware = require('../middlewares/auth');

const authConfig = require('../../config/auth')

const Product = require('../models/Product');

const router = express.Router();

router.use(authMiddleware);
const cors = require('cors')
router.use(cors);

// Verifica os dados passados pelo Token
// router.get('/', async (req, res) => {
//     return res.send({ 
//         id: req.userId,
//         access: req.access,
//         idAccess: req.idAccess
//     });
// })

router.post('/register', async (req, res) => {
    try {
        const product = await Product.create(req.body);
        
        return res.send({ product });
    } catch (err) {
        return res.status(400).send({ error: 'Falha no registro' });
    }
});

router.put('/update/:productId', async (req, res) => {
    try {

        const product = await Product.findByIdAndUpdate(req.params.productId, req.body, {new: true});
        
        return res.send({ product });
        
    } catch (err) {
        console.log(err);
        
        return res.status(400).send({ error: 'Falha no Registro' });
    }
});

router.get('/selectAll', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            var products = await Product.find();
        } else if (req.access === 'cliente') {
            const query = {client: req.idAccess}
            var products = await Product.find(query);
        }else if (req.access === 'filial') {
            const query = {subsidiary: req.idAccess}
            var products = await Product.find(query);
        }
        return res.send({ products });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.get('/selectById/:productId', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            var product = await Product.findById(req.params.productId);
        } else if (req.access === 'cliente') {
            const query = {client: req.idAccess}
            var product = await Product.findById(req.params.productId, query);
        }else if (req.access === 'filial') {
            const query = {subsidiary: req.idAccess}
            var product = await Product.findById(req.params.productId, query);
        }
        return res.send({ product });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.delete('/deleteById/:productId', async (req, res) => {
    try{
        if (req.access === 'adm') {
            var product = await Product.findByIdAndRemove(req.params.productId);
        } else if (req.access === 'cliente') {
            const query = {client: req.idAccess}
            var product = await Product.findByIdAndRemove(req.params.productId, query);
        }else if (req.access === 'filial') {
            const query = {subsidiary: req.idAccess}
            var product = await Product.findByIdAndRemove(req.params.productId, query);
        }

        return res.send({ msg: 'Deletado com Sucesso' });
    } catch (err) {
        return res.status(400).send({ error: 'Falha no Delete' });
    }
    
});

module.exports = app => app.use('/product', router);