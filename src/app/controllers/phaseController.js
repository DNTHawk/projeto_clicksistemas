const express = require('express');
const jwt = require('jsonwebtoken');
const authMiddleware = require('../middlewares/auth');

const authConfig = require('../../config/auth')

const Phase = require('../models/Phase');

const router = express.Router();

const cors = require('cors')
router.use(cors);
router.use(authMiddleware);

// Verifica os dados passados pelo Token
// router.get('/', async (req, res) => {
//     return res.send({ 
//         id: req.userId,
//         access: req.access,
//         idAccess: req.idAccess
//     });
// })

router.post('/register/opportunity', async (req, res) => {
    try {
        const { currentPhase, opportunity } = req.body

        const now = new Date();
        now.setHours(now.getHours() + 72);

        const phaseState = {
            currentPhase: currentPhase,
            opportunity : {
                name : opportunity.name,
                expiresAt: now
            }
        }

        const phase = await Phase.create(phaseState);

        return res.send({ phase });
    } catch (err) {
        console.log(err);
        
        return res.status(400).send({ error: 'Falha no registro' });
    }
});

router.put('/register/opportunity/updateById/:phaseId', async (req, res) => {
    try {
        const { currentPhase, opportunity } = req.body

        const phaseExpires = await Phase.findById(req.params.phaseId)

        const phaseState = {
            currentPhase: currentPhase,
            opportunity : {
                name : opportunity.name,
                expiresAt: phaseExpires.opportunity.expiresAt
            }
        }

        const phase = await Phase.findByIdAndUpdate(req.params.phaseId, phaseState, {new: true});

        return res.send({ phase });
    } catch (err) {
        console.log(err);
        
        return res.status(400).send({ error: 'Falha no registro' });
    }
});

module.exports = app => app.use('/phase', router);