const express = require('express');
const jwt = require('jsonwebtoken');
const authMiddleware = require('../middlewares/auth');

const authConfig = require('../../config/auth')

const AdmSystem = require('../models/AdmSystem');

const router = express.Router();

router.use(authMiddleware);

const cors = require('cors')
router.use(cors);

router.post('/register', async (req, res) => {
    try {
        if (req.access === 'adm') {
            const admSystem = await AdmSystem.create(req.body);

            return res.send({ admSystem });
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode cadastrar um Administrador' });
        }else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode cadastrar um Administrador' });
        }

    } catch (err) {
        console.log(err);
        
        return res.status(400).send({ error: 'Falha no registro' });
    }
});

router.put('/update/:admSystemId', async (req, res) => {
    try {
        if (req.access === 'adm') {
            const admSystem = await AdmSystem.findByIdAndUpdate(req.params.admSystemId, req.body, {new: true});

            return res.send({ admSystem });
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode alterar um Administrador' });
        }else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode alterar um Administrador' });
        }
        
    } catch (err) {
        return res.status(400).send({ error: 'Falha no Registro' });
    }
});

router.get('/selectAll', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            const admSystems = await AdmSystem.find();

            return res.send({ admSystems });
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode selecionar um Administrador' });
        }else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode selecionar um Administrador' });
        }
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.get('/selectById/:admSystemId', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            const admSystem = await AdmSystem.findById(req.params.admSystemId);

            return res.send({ admSystem });
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode selecionar um Administrador' });
        }else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode selecionar um Administrador' });
        }
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.delete('/deleteById/:admSystemId', async (req, res) => {
    try{
        if (req.access === 'adm') {
            await AdmSystem.findByIdAndRemove(req.params.admSystemId);
        } else if (req.access === 'cliente') {
            return res.status(400).send({ error: 'Somente Administrador pode deletar um Administrador' });
        }else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador pode deletar um Administrador' });
        }

        return res.send({ msg: 'Deletado com Sucesso' });
    } catch (err) {
        return res.status(400).send({ error: 'Falha no Delete' });
    }
    
});

module.exports = app => app.use('/admSystem', router);