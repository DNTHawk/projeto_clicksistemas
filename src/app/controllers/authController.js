const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const authConfig = require('../../config/auth')

const User = require('../models/User');

const router = express.Router();

const cors = require('cors')
router.use(cors);

function generateToken(params = {}){
    return token = jwt.sign(params, authConfig.secret, { expiresIn: 86400} )
}

// Rotas que não nescessitam de Token vão aqui \/

router.post('/authenticateLogin', async (req, res) => {
    const { email, password } = req.body;

    const user = await User.findOne({ email }).select('+password');

    if(!user)
        return res.status(400).send({ error: 'Usuário não encontrado' });

    if(!await bcrypt.compare(password, user.password))
        return res.status(400).send({ error: 'Senha Inválida' })

        user.password = undefined;

    if (user.access === 'adm') {
        idAccess = user.idAdm
    }else if( user.access === 'cliente' ){
        idAccess = user.idClient
    }else if( user.access === 'filial' ){
        idAccess = user.idSubsidiary
    }

    res.send({ 
        user, 
        token: generateToken({ id: user.id, access: user.access, idAccess: idAccess }),
    });
})

module.exports = app => app.use('/query', router);