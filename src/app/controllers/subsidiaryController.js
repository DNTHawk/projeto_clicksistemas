const express = require('express');
const authMiddleware = require('../middlewares/auth');
const jwt = require('jsonwebtoken');
const authConfig = require('../../config/auth')

const Subsidiary = require('../models/Subsidiary');

const router = express.Router();

router.use(authMiddleware);
const cors = require('cors')
router.use(cors);

function generateToken(params = {}){
    return token = jwt.sign(params, authConfig.secret, { expiresIn: 86400} )
}

// Verifica os dados passados pelo Token
// router.get('/', async (req, res) => {
//     return res.send({ 
//         id: req.userId,
//         access: req.access,
//         idAccess: req.idAccess
//     });
// })

router.post('/register', async (req, res) => {
    try {

        if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador e Cliente pode cadastrar uma Filial' });
        }
    
        var subsidiary = await Subsidiary.create(req.body)    
        
        return res.send({ subsidiary });

    } catch (err) {
        return res.status(400).send({ error: 'Falha no Registro' });
    }
});

router.put('/update/:subsidiaryId', async (req, res) => {
    try {
        if (req.access === 'filial') {
            return res.status(400).send({ error: 'Somente Administrador e Cliente pode alterar uma Filial' });
        }
            
        var subsidiary = await Subsidiary.findByIdAndUpdate(req.params.subsidiaryId,req.body, {new: true})  
        return res.send({ subsidiary });
    } catch (err) {
        return res.status(400).send({ error: 'Falha no Registro' });
    }
});

router.get('/selectAll', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            var subsidiarys = await Subsidiary.find().populate('client')
        } else if (req.access === 'cliente') {
            const query = {client: req.idAccess}
            var subsidiarys = await Subsidiary.find(query).populate('client')
        } else if (req.access === 'filial') {
            var subsidiarys = await Subsidiary.findById(req.idAccess).populate('client')
        }
        
        return res.send({ subsidiarys });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.get('/selectById/:subsidiaryId', async (req, res) =>{
    try {
        if (req.access === 'adm') {
            var subsidiary = await Subsidiary.findById(req.params.subsidiaryId).populate('client')
        } else if (req.access === 'cliente') {
            const query = {client: req.idAccess}
            var subsidiary = await Subsidiary.findById(req.params.subsidiaryId, query).populate('client')
        } else if (req.access === 'filial') {
            if (rqe.params.subsidiaryId === req.idAccess) {
                var subsidiary = await Subsidiary.findById(req.params.subsidiaryId).populate('client')
            }
        }
        
        return res.send({ subsidiary });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na Seleção' });
    }
});

router.delete('/deleteById/:subsidiaryId', async (req, res) => {
    try{
        if (req.access === 'adm') {
            await Subsidiary.findByIdAndRemove(req.params.subsidiaryId)
        } else if (req.access === 'cliente') {
            const query = {client: req.idAccess}
            await Subsidiary.findByIdAndRemove(req.params.subsidiaryId, query)
        } else if (req.access === 'filial') {
            return res.status(400).send({ error: 'Essa filial não pertence a esse cliente' });
        }

        return res.send({ msg: 'Deletado com Sucesso' });
    } catch (err) {
        return res.status(400).send({ error: 'Falha no Delete' });
    }
    
});

module.exports = app => app.use('/subsidiary', router);