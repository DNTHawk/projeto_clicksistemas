const mongoose = require('../../database');

const AdmSystemSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    createdAt:{
        type: Date,
        default: Date.now,
    }
})

const AdmSystem = mongoose.model('AdmSystem', AdmSystemSchema);

module.exports = AdmSystem;