const mongoose = require('../../database');

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    client: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client',
        require: true,
    },
    subsidiary: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subsidiary',
        require: true,
    },
    createdAt:{
        type: Date,
        default: Date.now,
    }
})

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;