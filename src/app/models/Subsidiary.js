const mongoose = require('../../database');

const SubsidiarySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    client: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client',
        require: true,
    },
    createdAt:{
        type: Date,
        default: Date.now,
    }
})

const Subsidiary = mongoose.model('Subsidiary', SubsidiarySchema);

module.exports = Subsidiary;