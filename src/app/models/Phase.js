const mongoose = require('../../database');

const PhaseSchema = new mongoose.Schema({
    currentPhase: {
        type: String,
        required: true
    },
    opportunity :{
        name: {
            type: String,
        },
        expiresAt:{
            type: Date,
        }
    },
    lead :{
        name: {
            type: String,
        },
        expiresAt:{
            type: Date,
        }
    },
    lead: {
        name: {
            type: String,
        },
        expiresAt:{
            type: Date,
        }
    },
    commercial: {
        name: {
            type: String,
        },
        expiresAt:{
            type: Date,
        }
    },
    budgeting: {
        name: {
            type: String,
        },
        expiresAt:{
            type: Date,
        }
    },
    proposal: {
        name: {
            type: String,
        },
        expiresAt:{
            type: Date,
        }
    },
    createdAt:{
        type: Date,
        default: Date.now,
    },
})

const Phase = mongoose.model('Phase', PhaseSchema);

module.exports = Phase;